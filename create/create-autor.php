<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>CREATE autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $id_autor= $_POST['id_autor'];
  $nombre_autor = $_POST['nombre_autor'];

  if (empty($id_autor)) {
    $error = true;
?>
  <p>Error, no se indico el ID del autor</p>
<?php
  }
  if (empty( $nombre_autor)) {
    $error = true;
?>
  <p>Error, no se indico el nombre del autor</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor
      from biblioteca.autor
      where id_autor = '".$id_autor."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 1) {
?>
  <p>Error, ya se encuentra registrado un autor con ID <?php echo $id_autor; ?></p>
<?php
    } else {
      $query = "insert into biblioteca.autor values('".$id_autor."', '".$nombre_autor."');";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del autor</p>
<?php
      } else {
?>
  <p>El autor con ID<?php echo $id_autor; ?> y nombre "<?php echo 
  $nombre_autor; ?>" ha sido guardado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="formulario-autor.php">Nuevo autor</a></li>
</ul>

</body>
</html>