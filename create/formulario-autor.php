<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<form action="create-autor.php" method="post">
<table>
  <caption>Información de autor</caption>
  <tbody>
    <tr>
      <th>ID </th>
      <td><input type="text" name="id_autor" /></td>
    </tr>
    <tr>
      <th>Nombre</th>
      <td><textarea name="nombre_autor"></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="CREATE" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>