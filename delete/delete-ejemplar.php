<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de ejemplares</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $clave_ejemplar = $_POST['clave_ejemplar'];
  if (empty($clave_ejemplar)) {
?>
  <p>Error, no se indico la clave del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar ,conservacion_ejemplar, isbn 
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún ejemplar con CLAVE <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
	  $isbn = $tupla['isbn'];

      $query = "delete from biblioteca.ejemplar where clave_ejemplar = '".$clave_ejemplar."';";
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      
      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de borrar el ejemplar</p>
<?php
      } else {
?>
  <p>El ejemplar con CLAVE <?php echo $clave_ejemplar; ?> 
  con  <?php echo $conservacion_ejemplar; ?> 
  y ISBN "<?php echo $isbn; ?>" fue borrado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>
