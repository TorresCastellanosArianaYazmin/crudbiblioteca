<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];
  $error = false;
  if (empty($clave_ejemplar)) {
    $error = true;
?>
  <p>Error, no se ha indicado la CLAVE del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar,isbn
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
      $error = true;
?>
  <p>No se ha encontrado algún ejemplar con clave <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
	  $isbn = $tupla['isbn'];
?>
<table>
  <caption>Información de ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>Conservación</th>
      <td><?php echo $conservacion_ejemplar; ?></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>Titulo</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro as L
        inner join biblioteca.ejemplar E
          on (L.isbn = E.isbn and E.isbn = '".$isbn."'
		   and E.clave_ejemplar = '".$clave_ejemplar."' 
		   and 
		  E.conservacion_ejemplar = '".$conservacion_ejemplar."' );";

      $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libro) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libro, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <tr>
      <th>Autor/es</th>
      <td>
<?php
      $query = "select nombre_autor
        from biblioteca.autor as A
        inner join biblioteca.libro_autor as LA
        on (LA.id_autor = A.id_autor and LA.isbn = '".$isbn."')
        inner join biblioteca.ejemplar as E
        on (LA.isbn = E.isbn and E.isbn = '".$isbn."')
        where E.isbn = '".$isbn."' and E.clave_ejemplar='".$clave_ejemplar."';";

      $autores= pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autores</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          $id_autor = $tupla['id_autor'];
          $nombre_autor = $tupla['nombre_autor'];
?>
          <li><?php echo $nombre_autor; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-ejemplar.php" method="post">
  <input type="hidden" name="clave_ejemplar" value="<?php echo $clave_ejemplar; ?>" />
  <p>¿Está seguro/a de eliminar este ejemplar?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán los datos del ejemplar.
  </p>
</form>

<form action="ejemplares.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>
