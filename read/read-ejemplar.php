<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];

  if (empty($clave_ejemplar)) {
?>
  <p>Error, no se ha indicado la clave  del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar,isbn 
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún libro con CLAVE <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
	  $isbn = $tupla['isbn'];
?>
<table>
  <caption>Información del ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>Conservación</th>
      <td><?php echo $conservacion_ejemplar; ?></td>
    </tr>
     <tr>
      <th>ISBN</th>
      <td><?php echo $isbn ?></td>
    </tr>
    <tr>
     <tr>
      <th>Titulo</th>
      <td>
<?php
      $query = "select  L.titulo_libro
		 from biblioteca.libro as L
        inner join biblioteca.ejemplar as E
          on (L.isbn = E.isbn)
		where E.isbn = '".$isbn."' and E.clave_ejemplar = '".$clave_ejemplar."' and  E.conservacion_ejemplar= '".$conservacion_ejemplar."';";

      $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libro) == 0) {
?>
        <p>Sin libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libro, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <th>Autor/es</th>
      <td>
<?php
      
$query = "select nombre_autor
        from biblioteca.autor as A 
	inner join biblioteca.libro_autor as LA 
	on (A.id_autor=LA.id_autor)
        inner join biblioteca.ejemplar as E

          on (LA.isbn=E.isbn  and E.clave_ejemplar= '".$clave_ejemplar."' and E.isbn= '".$isbn."');";

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
	     }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>